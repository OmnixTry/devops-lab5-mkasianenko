terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }

  required_version = ">=1.2.0"

}

# Configure the AWS Provider
provider "aws" {
  region = "eu-west-2"
}

resource "aws_instance" "app_server" {
  count = 2

  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.instance_type
  key_name               = "mkasianenko-key"
  vpc_security_group_ids = [data.aws_security_group.selected.id]

  tags = {
    Name = "MKasianenkoAppServerInstanceRenamed-${count.index}"
  }
}
